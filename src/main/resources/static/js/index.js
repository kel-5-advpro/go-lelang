// === countdown function starts here ===
// Set the date we're counting down to
function countDownProgram(waktu, idBarang) {
    var countDownDate = new Date(waktu).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        var ret = days + ":" + hours + ":"
            + minutes + ":" + seconds + ":";
        var idnya = "count-down-" + idBarang;
        document.getElementById(idnya).innerHTML = ret;
        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById(idnya).innerHTML = "EXPIRED";
        }
    }, 1000);
}
// === count down ends here ===

