package home.golelang.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index() {

        return "home";
    }

    @GetMapping("/program")
    public String program(@RequestParam(name = "id", required = true)
                                      String id, Model model) {
        return "program";

    }

    @GetMapping("/about")
    public String about() {

        return "about";
    }

    @GetMapping("/profile")
    public String profile() {

        return "profile";
    }

    @GetMapping("/help")
    public String help() {

        return "help";
    }

    @GetMapping("/winners")
    public String winners() {

        return "winners";
    }

    @GetMapping("/signup")
    public String signup() {

        return "signup";
    }

    @GetMapping("/login")
    public String login() {

        return "signin";
    }

}
