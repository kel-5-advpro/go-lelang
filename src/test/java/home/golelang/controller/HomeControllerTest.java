package home.golelang.controller;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = HomeController.class)
public class HomeControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Test
    public void indexTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.index(), "home");
    }

    @Test
    public void programTest() throws Exception{
        mockMvc.perform(get("/program").param("id","2")).andExpect(status().isOk());
    }

    @Test
    public void profileTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.profile(), "profile");
    }

    @Test
    public void aboutTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.about(), "about");
    }

    @Test
    public void helpTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.help(), "help");
    }


    @Test
    public void winnersTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.winners(), "winners");
    }

    @Test
    public void logInTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.login(), "signin");
    }

    @Test
    public void signUpTest() {
        HomeController homeController = new HomeController();
        assertEquals(homeController.signup(), "signup");
    }
}