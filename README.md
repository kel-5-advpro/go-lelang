```bash
Kelompok    : 5
Kelas       : Advance Programming - B
Anggota     : Aloysius K. M. (1706040025)
              Aulia Rosyida (1706025346)
              Fakhira Devina (1706979221)
              Ferro Geraldi H. (1706028612)
              M. Feril Bagus P. (1706075054)

```
## Repositories
UI<br>
https://gitlab.com/kel-5-advpro/go-lelang <br>
API<br>
https://gitlab.com/kel-5-advpro/microservice-barang <br>
Authentication<br>
https://gitlab.com/kel-5-advpro/auth-lelang 

## Link Heroku
Go-Lelang<br>
https://go-lelang.herokuapp.com/ <br>
API<br>
http://microservice-barang.herokuapp.com/ <br>
Authentication<br>
http://auth-lelang.herokuapp.com/

## Endpoint
API <br>
https://docs.google.com/document/d/1F9N8t3WPnbUx0Ha5bc_gKwHXHkUk1jar0lnwHOV2tQc/edit
<br> Authentication <br>
https://docs.google.com/document/d/1oHNdBWwl6f5SfDoZCg4MLatxEuqFDQ4zYsOrpZFKrSY/edit

## Status Aplikasi
[![Pipeline](https://gitlab.com/kel-5-advpro/go-lelang/badges/master/pipeline.svg)](https://gitlab.com/kel-5-advpro/go-lelang/commits/master)
[![Coverage](https://gitlab.com/kel-5-advpro/go-lelang/badges/master/coverage.svg)](https://kel-5-advpro.gitlab.io/go-lelang/index.html)

## Deskripsi Projek <br>
http://bit.ly/descGoLelang

 